{% docs __overview__ %}

## Data Documentation for GitLab

Please see 
* [The Data Team page of the Handbook](https://about.gitlab.com/handbook/business-ops/data-team/)
* [SQL Style Guide](https://about.gitlab.com/handbook/business-ops/data-team/platform/sql-style-guide)

## What are dbt docs? 

Here we create documentation for each individual dbt model. It is stored in the same pattern as the models are. 

For any other questions, please reach out to the Data Team. 

{% enddocs %}