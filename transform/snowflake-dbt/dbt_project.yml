# ======
# Project Details
# ======
name: 'gitlab_snowflake'
version: '1.0'
profile: 'gitlab-snowflake'
require-dbt-version: ">=0.16.0"


# ======
# File Path Configs
# ======
source-paths: ["models"]
test-paths: ["tests"]
data-paths: ["data"]
macro-paths: ["macros"]
target-path: "target"
clean-targets:
    - "target"
    - "dbt_modules"

# ======
# Snowflake Configs
# ======
quoting:
    database: true
    identifier: false
    schema: false
query-comment: "{{ query_comment(node) }}"

# ======
# Run Hooks
# ======
on-run-start:
    - "{{ dbt_logging_start('on run start hooks') }}"
    - "{{ resume_warehouse(var('resume_warehouse', false), var('warehouse_name')) }}"
    - "{{ create_udfs() }}"

on-run-end:
    - "{{ dbt_logging_start('on run end hooks') }}"
    - "{{ grant_usage_to_schemas() }}"
    - "drop schema if exists {{ generate_schema_name('temporary') }}"
    - "{{ suspend_warehouse(var('suspend_warehouse', false), var('warehouse_name')) }}"

# ======
# Seed Configs
# ======
seeds:
  enabled: true
  schema: staging
  quote_columns: False

  gitlab_snowflake:
    zuora_country_geographic_region:
      schema: analytics
    gitlab_release_schedule:
      column_types:
        major_minor_version: varchar


# ======
# Model Configs
# ======
models:
  pre-hook:
    - "{{ logging.log_model_start_event() }}"
  post-hook:
    - "{{ logging.log_model_end_event() }}"
  vars:
    database: "raw"
    warehouse_name: "{{ env_var('SNOWFLAKE_TRANSFORM_WAREHOUSE') }}"

  # Logging Package
  logging:
    schema: meta
    post-hook:
      - "grant select on {{this}} to role reporter"

  # Snowplow Package
  snowplow:
    schema: "snowplow_{{ var('year', run_started_at.strftime('%Y')) }}_{{ var('month', run_started_at.strftime('%m')) }}"
    post-hook:
      - "grant usage on schema {{this.schema}} to role snowplow"
      - "grant select on {{this}} to role snowplow"
    tags: ["product"]
    vars:
      'snowplow:use_fivetran_interface': false
      'snowplow:events': "{{ref('snowplow_unnested_events')}}"
      'snowplow:context:web_page': "{{ref('snowplow_web_page')}}"
      'snowplow:context:performance_timing': false
      'snowplow:context:useragent': false
      'snowplow:timezone': 'America/New_York'
      'snowplow:page_ping_frequency': 30
      'snowplow:app_ids': ['gitlab', 'about', 'gitlab_customers']
      'snowplow:pass_through_columns': ['glm_source','cf_formid','cf_elementid','cf_nodename','cf_type','cf_elementclasses','cf_value','sf_formid','sf_formclasses','sf_elements','ff_formid','ff_elementid','ff_nodename','ff_elementtype','ff_elementclasses','ff_value','lc_elementid','lc_elementclasses','lc_elementtarget','lc_targeturl','lc_elementcontent','tt_category','tt_variable','tt_timing','tt_label']

  # Snowflake Spend Package
  snowflake_spend:
    materialized: table
    xf:
      schema: analytics

  # GitLab Models
  gitlab_snowflake:
    materialized: view

    # Dimensional Structure
    sources:
    
      airflow:
        materialized: table
        schema: airflow

      customers:
        schema: customers
        sensitive:
          schema: sensitive
          secure: true

      dbt:
        materialized: table
        schema: dbt
    
      engineering:
        materialized: table

      gitlab_data_yaml:
        schema: gitlab_data_yaml

      gitlab_dotcom:
        tags: ["product"]
        materialized: table
        schema: staging
      
      gitter:
        enabled: false

      google_analytics_360:
        materialized: table
        schema: google_analytics_360

      greenhouse:
        schema: greenhouse

      handbook:
        schema: handbook

      license:
        schema: license_db

      netsuite:
        schema: netsuite

      qualtrics:
        materialized: table
        schema: qualtrics

      sfdc:
        schema: sfdc
        sensitive:
          schema: sensitive
          secure: true

      sheetload:
        schema: sheetload

      snowflake:
        schema: snowflake

      version:
        tags: ["product"]
        materialized: table
        schema: version_db

      zendesk:
        schema: zendesk

      zuora:
        schema: zuora


    staging:
      schema: staging
      materialized: table

      customers:
        xf:
          schema: analytics

      engineering:
        materialized: table

      gitlab_data_yaml:
        xf:
          schema: analytics

      gitlab_dotcom:
        tags: ["product"]
        materialized: table
        schema: analytics

      gitter:
        enabled: false
        materialized: table

      google_analytics_360:
        xf:
          schema: analytics

      greenhouse:
        xf:
          schema: sensitive
          secure: true
        rpt:
          schema: analytics

      handbook:
        rpt:
          schema: analytics
        xf:
          schema: analytics

      license:
        materialized: view

      netsuite:
        xf:
          schema: analytics

      qualtrics:
        schema: analytics

      sfdc:
        xf:
          schema: analytics

      snowflake:
          schema: analytics
          materialized: view

      version:
        tags: ["product"]
        xf:
          schema: analytics
        ephemeral:
          schema: analytics

      zuora:
        xf:
          schema: analytics
    
    marts:  
      
      arr:  
        materialized: table  

      product_kpis:
        materialized: table 

      exports:
        materialized: view
        schema: exports 


    # Legacy Structure
    bamboohr:
      schema: sensitive
      secure: true
      base:
        materialized: table

    date:
      materialized: table

    netsuite:
      materialized: table
      base:
        schema: staging

    pipe2spend:
      enabled: false
      materialized: table

    poc:
      tags: ["poc"]
      enabled: true
      materialized: table
      schema: analytics

    retention:
      materialized: table

    smau_events:
      tags: ["product"]
      materialized: table

    snowplow:
      tags: ["product"]
      xf:
        materialized: table

    snowplow_combined:
      tags: ["product"]


# ======
# Snapshot Configs
# ======
snapshots:
  gitlab_snowflake:
    target_database: "{{ env_var('SNOWFLAKE_SNAPSHOT_DATABASE') }}"
    target_schema: "snapshots"
    transient: false

    customers:
      tags: ["daily"]

    gitlab_dotcom:
      tags: ["daily"]

    license:
      tags: ["daily"]

    netsuite:
      tags: ["daily"]

    sfdc:
      tags: ["daily"]

    sheetload:
      tags: ["daily"]

    zuora:
      tags: ["daily"]
